const
	Play = require('./gplay'),
	express = require('express'),
	bodyParser = require('body-parser');

/*
Express server
 */
const app = express();
// app.get('/', (req, res) => res.redirect('maintenance.html'));
app.use(express.static('public'));
app.use(bodyParser.json());
app.post('/search', (request, response) => Play.search(request.body).then(results => response.json(results)));
app.listen(3030);
