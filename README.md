# KPlaySearch
KPlaySearch (KaKi's Google Play search engine) allows you to perform advanced G-Play search, using filters and sorting features. [Demo](https://playsearch.kaki87.net/)

## Getting Started

### Prerequisites
- NodeJS
- Yarn

### Installing
```
yarn install
yarn start
```

## Built With
Node modules :
- [Express](https://expressjs.com/) (with [body-parser](https://github.com/expressjs/body-parser)) - Back end framework
- [google-play-scraper](https://github.com/facundoolano/google-play-scraper) - GPlay API

Web components :
- [UIkit](https://getuikit.com/) - Front end framework
- [FontAwesome](https://fontawesome.com/) - Icon pack
- [JavaScript Cookie](https://github.com/js-cookie/js-cookie) - JS cookie API

## Authors
**KaKi87 (Tiana Lemesle)** - *Initial work*

Special thanks to ribt, DotDotDot, TheDevKiller, Fomys and Guysmow from the *french* [CQSCMQPI](https://discord.gg/79JjWTF) Discord server.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details